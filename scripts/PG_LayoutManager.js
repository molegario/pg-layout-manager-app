// Scope all code so that we can control what seeps into the window object
(function () {
    var PG_LayoutManager = function() {
    };

    /**
     * X2O Object properties are typed, and a few components of the system are sensitive to this type (Channel Designer's property list dialog
     * will display a specific input field for certain types, for example). Values not specified in this list will be tolerated, but have no
     * special effect in the system.
     *
     * Each value is defined here with its name followed by the input control shown by Channel Designer and notes if applicable, separated by "//";
     * names are case-insensitive.
     *
     * So although this documentation presents a data structure, this is in fact a list of all possible values (that also explains why all "properties"
     * below are presented with a type of {string}, regardless of the information they carry) for an X2O object's custom property type.
     *
     * @property {string} Audio         The identifier of an audio asset.   <br>A dialog that navigates the current network, restricted to audio assets.
     * @property {string} Bool          A value of "True" or "False".       <br> A drop-down with solely "True" or "False" as options.
     * @property {string} Boolean       A value of "True" or "False".       <br> A drop-down with solely "True" or "False" as options.
     * @property {string} Channel       A channel in the X2O Platform.      <br> A channel selector dialog to locate and select a channel.
     * @property {string} Color         Represents a color value (RGB).     <br> A color-picker, where the user can also set an RGB value manually.
     * @property {string} Colorlist     Represents a color value (RBG).     <br> A list of pre-defined colors to pick from doubled with a manual entry textbox.
     * @property {string} Contentfolder Path to a content folder.           <br> A treeview-based content folder browser/selector.
     * @property {string} Customeditor  Specialized text content.           <br> A dialog containing an editor as defined by the object. (See x2o.ObjectBase API documentation)
     * @property {string} DataDocument  The identifier of a known data document. <br> A dialog that allows navigating known data documents.
     * @property {string} Datafeed      The identifier of a known datafeed. <br> A dialog that allows navigating known datafeeds.
     * @property {string} Excel         The identifier of an Excel asset.   <br> A dialog that navigates the current network, restricted to Excel assets.
     * @property {string} File          The identifier of a file asset.     <br> A dialog that navigates the current network, restricted to file assets.
     * @property {string} Flash         The identifier of a flash asset.    <br> A dialog that navigates the current network, restricted to Flash assets.
     * @property {string} Flipbook      The identifier of a flipbook asset. <br> A dialog that navigates the current network, restricted to flipbook assets.
     * @property {string} Float         A floating point value.             <br> A textbox restricted to floating point number entry.
     * @property {string} Format        A temperature format indicator.     <br> A dropdown allowing for one of &deg;C or &deg;F or player setting.
     * @property {string} Font          A JSON-encoded font definition      <br> Although this field can be manually edited, it interacts with the font
     *                                                                           toolbar in Channel Designer; the object properties dialog only provides a
     *                                                                           raw text editor for this data.
     * @property {string} Image         The identifier of an image asset.   <br> A dialog that navigates the current network, restricted to image assets.
     * @property {string} Integer       An integer value.                   <br> A textbox restricted to numeric entry.
     * @property {string} Language      A language code identifier.         <br> A drop-down allowing selection from a few pre-defined languages, or player setting.
     * @property {string} Misc          The identifier of a misc asset.     <br> A dialog that navigates the current network, restricted to misc assets.
     * @property {string} Objectname    A valid object name.                <br> A textbox which respects Channel Designer object name restrictions.
     *
     * @property {string} Option{[values]} One of a set of pre-defined values.<br>
     *            A drop-down out of a possible values. <p>
     *            Values are to be supplied directly in the property typename, using hash keys as returned values and hash values as
     *            display text; {"1": "Today", "2": "Tomorrow"} would yield a drop-down with "Today" and "Tomorrow" as display entries
     *            and "1" and "2" as corresponding values. **It is highly recommended to supply this value as `'option' +
     *            JSON.stringify(object)`** as it guarantees proper JSON-encoding of the values.
     *
     * @property {string} Password      A string but displayed with asterisks.  <br> A textbox for freeform entry, that doesn't show the text.
     * @property {string} PDF           The identifier of a pdf asset.       <br> A dialog that navigates the current network, restricted to PDF assets.
     * @property {string} Playlist      An identifier to a playlist.         <br> A dialog tha navigates playlists for the current network.
     * @property {string} Powerpoint    The identifier of a PowerPoint asset.<br> A dialog that navigates the current network, restricted to PowerPoint assets.
     * @property {string} Richtext      Text value meant for rich content.<br> A dialog that offers an editor for rich text.
     * @property {string} Stretchmode   One of the possible stretchmodes.    <br> A drop-down with pre-defined values: keepaspect, stretch, originalsize.
     * @property {string} String        A string literal.                    <br> A textbox for freeform entry.
     * @property {string} Twitter       Possible Twitter operating modes.    <br> A drop-down with search modes by hash tag, by user account or freeform query.
     * @property {string} Video         The identifier of a video asset.     <br> A dialog that navigates the current network, restricted to video assets.
     * @property {string} Wayfinder     The identifier of a wayfinder asset. <br> A dialog that navigates the current network, restricted to wayfinder assets.
     */
    var objectProperties = [
        {
            name: "UserName",
            type: "string",
            description: "Enter the user name",
            extendedDescription: "User name is used to display the message \"Hello {{UserName}}\"",
            value: 'World',
            mainProperty: true
        },
        {
            name: "Color",
            type: "Color",
            description: "Font color",
            extendedDescription: "Color of the displayed text",
            value: '#ffffff'
        }
    ];

    // Inheritance setup
    PG_LayoutManager.inheritsFrom(x2o.ObjectBase);

    /**
     * This method is called when internal property values of the object have changed, as a result of:
     *  - Channel Designer's property dialog being used to modify properties at design-time
     *  - An object being brought to life at run-time
     *  - A data-bound property being updated automagically
     *  - Anytime an external entity calls `public.setProperties` on the object (see {@link x2o.ObjectBase~DefaultPublicInterface})
     *
     * @see x2o.ObjectBase#onPropertiesUpdated
     *
     * @param {string[]} updatedProps   A list containing the names of all properties affected by the operation
     * @param {string[]} updatedData    A list containing the names of all data-bound properties affected by the operation
     */
    PG_LayoutManager.prototype.onPropertiesUpdated = function (updatedProps, updated_data) {
        if(updatedProps.indexOf('UserName') > -1) {
            this.setUserName(this.properties_.values['UserName']);
        }

        if(updatedProps.indexOf('Color') > -1) {
            this.setColor(this.properties_.values['Color']);
        }
    };

    /**
     * Set text color for the message.
     *
     * @param color The new color value
     */
    PG_LayoutManager.prototype.setColor = function(color) {
        $('.message').css({
            color: color
        });
    };

    /**
     * Update the user name displayed
     *
     * @param name  The name to display
     */
    PG_LayoutManager.prototype.setUserName = function(name) {
        // Update displayed name
        $('#nameProperty').text(name);
    };

    /**
     * Object initialisation
     */
    PG_LayoutManager.prototype.init = function () {
        var self = this;
        var deferred = $.Deferred();
    
        /**
         * See Objects API documentation (Platform 
         */
        self.setInitOptions({
            showHeader: true,           // Default value
            title: {
                value: 'Title'          // Default value
            },
            backgroundStyle: 'object'   // Default value
        });
        
        $.when(this.construct(
            window.name,
            objectProperties
        )).then(function () {
            // Raises an event to signify that this object is ready to be used.
            // Only call this function once the object has finished initializing for 
            // the first time.
            // 
            // A channel will keep track of all existing object
            // and fire 'Channel:AllObjectsReady' when all objects have triggered this event.
            self.notifyObjectReady({
                name: PG_LayoutManager
            });

            deferred.resolve();
        });

        return deferred.promise();
    };

    // Export object from namespace
    window.PG_LayoutManager = new PG_LayoutManager();

    //Bootstrap
    $(function () {
        $.when(window.PG_LayoutManager.init()).then(function () {
            // POST-CONSTRUCTION CODE
            var app = $.sammy('#main', function () {
                //X2O vars
                var client = false,
                cartridgeId = 'f5ffdd31-7e9c-49c8-8934-26b4dbb749a6',
                restAPIURL = window.location.protocol + '//' + window.location.host + '/XmanagerWeb/rest/v1';
                epoxy_events_handler = {
                    connected: function () {
                        console.log("Epoxy client connected - client handler!");
                    },
                    disconnected: function() {
                        console.log("Epoxy client disconnected - client handler!");
                        client.unregisterClient(cartridgeId, epoxy_events_handler);
                    },
                    incoming: function (msg) {
                        console.log("Epoxy client data push received - client handler!");
                        app.trigger('x2o.epoxy.push', msg);
                    }
                };

                //USE - Sammy Templates
                this.use('Template');
                this.use('Storage');

                //SESSION STATE
                var pgcookie = new Sammy.Store({
                    name: 'pglayoutmanager',
                    type: 'cookie'
                });

                //INSTANCE Constants
                var pgconstants = new Sammy.Store({
                    name: 'pglayoutmanagerconstants',
                    type: 'memory'
                });

                //INSTANCE STATE
                var pgstate = new Sammy.Store({
                    name: 'pglayoutmanager',
                    type: 'memory'
                });

                //INSTANCE tempstate
                var pgtempstate = new Sammy.Store({
                    name: 'pglayoutmanagertempstate',
                    type: 'memory'
                });

                //Check logged in
                function checkLoggedIn (callback) {
                    //persist login from cookie
                    if(!pgstate.exists('current_user') && pgcookie.exists('current_user')) {
                        pgstate.set('current_user', pgcookie.get('current_user'));
                    }
                    //toggle state of logout button
                    if (pgstate.exists('current_user')) {
                        $('#app-logout').removeClass('d-none');
                    } else {
                        $('#app-logout').addClass('d-none');
                    }
                    callback();
                }

                //implement around of checkloggedin at every request
                this.around(checkLoggedIn);

                //ROUTE: #/, WEBROOT
                this.get('#/', function (context) {
                    console.log('DEST::ROOT');
                    if(pgstate.exists('current_user')) {
                        app.trigger('log.in');
                    }
                    context.partial('templates/login.template', {
                        item: {
                            message: 'Click \'Enter\' to proceed.'
                        }
                    });
                });
                
                //POST: #/login, Login POST
                this.post('#/login', function (context) {
                    var current_user = _.extend({
                        Username: 'TESTUSER'
                    }, context.params);

                    //INIT TRACKED STATES
                    pgstate.set('current_user', current_user);
                    pgcookie.set('current_user', current_user);
                    app.trigger('log.in');
                    return false;
                })

                //GET: #/logout, Logout GET
                this.get('#/logout', function (context) {
                    app.trigger('log.out');
                    return false;
                });

                //EVENT log.in
                this.bind('log.in', function () {

                    //define hierarchy structure
                    pgconstants.set('hierarchy_types', [
                        {
                            name: '#',
                            max_children: 1,
                            valid_children: ['Unassigned']
                        },{
                            name: 'Unassigned',
                            valid_children: ['Screen'],
                            icon: 'far fa-question-circle'
                        },{
                            name: 'Building',
                            valid_children: ['Floor'],
                            icon: 'far fa-building'
                        },{
                            name: 'Floor',
                            valid_children: ['Room'],
                            icon: 'fas fa-layer-group'
                        },{
                            name: 'Room',
                            valid_children: ['Area'],
                            icon: 'fas fa-door-open'
                        },{
                            name: 'Area',
                            valid_children: ['Screen'],
                            icon: 'fas fa-chalkboard-teacher'
                        },{
                            name: 'Screen',
                            valid_children: [],
                            icon: 'fas fa-desktop'
                        }
                    ]);
                    //set constant - highest tier - BUILDING
                    pgconstants.set('top_management_level', 'Floor');
                    
                    //init - no temp save state
                    pgtempstate.set('current_network_layout', false);
                    //redirect to mainpage
                    this.redirect('#/sessions');
                });

                //EVENT log.out
                this.bind('log.out', function () {
                    client.sendCommand({
                        "Id": cartridgeId
                    },{
                        "Event": "UNREGISTER"
                    }).then(function (response) {
                        console.log(response);
                    });

                    $('#logoutModal').modal('hide');
                    pgstate.clearAll();
                    pgtempstate.clearAll();
                    pgcookie.clearAll();
                    
                    //default to login page (root)
                    this.redirect('#/');
                });

                //Format save structure to tree data
                function recursiveFormatter (children, no_screens) {
                    return _.reduce(children, function (acc, itm, idx) {
                        var scr_attr, found_scr_info;
                        var out = _.extend(itm, {
                            "text": itm.Name,
                            "type": itm.Type ? itm.Type : 'Screen',
                            "state": {
                                "opened": true
                            },
                            "li_attr": {
                                Id: itm.Id ? itm.Id : ""
                            }
                        });
                        if (itm.Type && itm.Type === 'Screen') {
                            found_scr_info = _.findWhere(pgstate.get('screens_inventory'), {
                                Name: itm.Name
                            });
                            if(found_scr_info) {
                                scr_attr = {
                                    Status: 'PG-' + found_scr_info.State
                                }
                            } else {
                                scr_attr = {
                                    Status: 'PG-ScreenNotFound'
                                }
                            }
                            out.li_attr = _.extend(out.li_attr, scr_attr);
                        }
                        //no screens - reset assign
                        if(itm.Children && (itm.Children.length > 0)) {
                            if(no_screens && itm.Type === 'Area') {
                                out.children = [];
                                out.Children = [];
                            } else {
                                out.children = recursiveFormatter(itm.Children, no_screens);
                            }
                        } else {
                            out.children = [];
                        }    
                        return acc.concat(out);
                    }, []);
                };

                //format tree data to save structure
                function recursiveReformatter (children) {
                    return _.reduce(children, function (acc, itm, idx) {
                        var out = _.extend({}, {
                            Id: itm.li_attr && itm.li_attr.Id ? itm.li_attr.Id : "",
                            Name: itm.text,
                            Type: itm.type,
                        });
                        if(itm.children && (itm.children.length > 0)) {
                            out.Children = recursiveReformatter(itm.children)
                        } else {
                            out.Children = [];
                        }
                        return acc.concat(out);
                    },[]);
                }

                //get all screens in a tree
                function getAssignedScreens (children) {
                    return _.reduce(children, function (acc, itm, idx) {
                        if (itm.Children && (itm.Children.length > 0)) {
                            acc.push(getAssignedScreens(itm.Children));
                        } else {
                            acc.push(itm);
                        }
                        return acc;
                    }, []);
                };

                //used to normalize all structures to building root
                function getPrecursors(top_level_type) {
                    var start = _.find(pgconstants.get('hierarchy_types'), function (itm, idx) {
                        return _.contains(itm.valid_children, pgconstants.get('top_management_level'));
                    });
                    var names_list = [], checkname=start.name;
                    
                    var n = 0;
                    while (checkname !== top_level_type && n < pgconstants.get('hierarchy_types').length) {
                        var check = _.findWhere(pgconstants.get('hierarchy_types'), {
                            name: checkname
                        });
                        checkname = check.valid_children[0];
                        names_list.push(check.name);
                        n++;
                    }
                    return names_list;
                };

                //INIT actions #/sessions
                function InitSessionsPage () {
                    //INIT sessions
                    //TREE UI
                    $('#current-tree').jstree({
                        "core" : {
                            "check_callback" : true,
                            data: pgtempstate.get('display_layout')
                        },
                        "contextmenu": {
                            items: function (node) {
                                var actions = {}, node_type = _.findWhere(pgconstants.get('hierarchy_types'), {
                                    name: node.type
                                });
                                var top_level = _.find(pgconstants.get('hierarchy_types'), function (itm, idx) {
                                    return itm.valid_children[0] === pgconstants.get('top_management_level');
                                });
                                if(node_type.name === top_level.name) {
                                    actions.save = {
                                        "label": "Save to Network",
                                        "action": function (data) {
                                            var ref = $.jstree.reference(data.reference);
                                            var out = [],
                                                full_layout = recursiveReformatter(ref.get_json());
                                            out.push(full_layout[0]);
                                            pgtempstate.clear('current_network_layout');

                                            //save epoxy
                                            client.sendCommand({
                                                "Id": cartridgeId
                                            }, {
                                                "Event": "SAVE_LAYOUT",
                                                "Layout": out
                                            }).then(function (response) {
                                                if(response.success) {
                                                    app.refresh();
                                                } else {
                                                    context.partial('templates/error.template', {
                                                        item: {
                                                            message: 'Save Failed.'
                                                        }
                                                    });
                                                }
                                            });
                                        },
                                        "icon": "fas fa-sign-in-alt"
                                    };
                                }
                                if(node_type.valid_children[0] !== 'Screen' && node_type.name !== 'Screen') {
                                    //create if not root and screen
                                    actions.create = {
                                        "label": "Create " + node_type.valid_children[0],
                                        "action": function (data) {
                                            var ref = $.jstree.reference(data.reference);
                                            sel = ref.get_selected();
                                            if(!sel.length) { return false; }
                                            sel = sel[0];
                                            sel = ref.create_node(sel, {"type": node_type.valid_children[0]});
                                            if(sel) {
                                                ref.edit(sel);
                                            }
                                        },
                                        "icon": "far fa-plus-square"
                                    };
                                }
                                if(node_type.name !== 'Screen') {
                                    actions.rename = {
                                        "label": "Rename " + node_type.name,
                                        "action": function (data) {
                                            var inst = $.jstree.reference(data.reference);
                                            obj = inst.get_node(data.reference);
                                            inst.edit(obj);
                                        },
                                        "icon": "far fa-edit"
                                    };
                                    if(node_type.name !== top_level.name) {
                                        actions.delete = {
                                            "label": "Delete " + node_type.name,
                                            "action": function (data) {
                                                var top = $.jstree.reference($('#current-tree'));
                                                var ref = $.jstree.reference(data.reference),
                                                sel = ref.get_selected();
                                                if(!sel.length) { return false; }
                                                ref.delete_node(sel);
                                                var out = [], 
                                                    layout = recursiveReformatter(top.get_json());
                                                out.push(layout[0]);
                                                pgtempstate.set('current_network_layout', out);
                                                app.refresh();
                                            },
                                            "icon": "fa fa-times"
                                        }
                                    }
                                } else {
                                    actions.unassign = {
                                        "label": "Unassign " + node.text,
                                        "action": function (data) {
                                            var top = $.jstree.reference($('#current-tree'));
                                            var ref = $.jstree.reference(data.reference),
                                            sel = ref.get_selected();
                                            if(!sel.length) { return false; }
                                            ref.delete_node(sel);
                                            var out = [], 
                                                layout = recursiveReformatter(top.get_json());
                                            out.push(layout[0]);
                                            pgtempstate.set('current_network_layout', out);
                                            app.refresh();
                                        },
                                        "icon": "fa fa-times"
                                    }
                            }
                                return actions;
                            }
                        },
                        "types": _.indexBy(pgconstants.get('hierarchy_types'), function (itm, idx) {
                            return itm.name;
                        }),
                        "plugins" : [
                            "dnd",
                            "changed",
                            "contextmenu",
                            "types"
                        ]
                    }).on('changed.jstree', function (evt, data) {
                        console.log(data);
                    });

                    //UNASSIGNED UI
                    $('#unassigned-tree').jstree({
                        "core" : {
                            "check_callback" : true,
                            data: pgtempstate.get('display_unassigned')
                        },
                        "contextmenu": {
                            items: function (node) {
                                var actions = {}, node_type = _.findWhere(pgconstants.get('hierarchy_types'), {
                                    name: node.type
                                });
                                if(node_type.name === 'Unassigned') {
                                    actions.unassign_all = {
                                        "label": "Unassign All Players",
                                        "action": function () {
                                            pgtempstate.set('current_network_layout', recursiveFormatter(pgtempstate.get('current_network_layout'), true));
                                            app.refresh();
                                        },
                                        "icon": "fa fa-recycle"
                                    };
                                }
                                return actions;
                            }
                        },
                        "types": _.indexBy(pgconstants.get('hierarchy_types'), function (itm, idx) {
                            return itm.name;
                        }),
                        "plugins" : [
                            "dnd",
                            "changed",
                            "contextmenu",
                            "types"
                        ]
                    }).on('changed.jstree', function (evt, data) {
                        console.log(data);
                    });

                };

                //RENDER #/sessions
                function RenderSessionsPage (context, response) {
                    //RENDER
                    //precursors + layout
                    var network_layout = [];

                    //sync to current server layout or currently edited
                    if(pgtempstate.get('current_network_layout')) {
                        network_layout = recursiveFormatter(pgtempstate.get('current_network_layout'));
                    } else {
                        network_layout = recursiveFormatter(response.data); //temporary - to be restored
                        pgtempstate.set('current_network_layout', network_layout);
                    }

                    //get top level type
                    var top_level_type = _.first(network_layout) ? _.first(network_layout).Type : 'Floor';
                    var precursors = getPrecursors(top_level_type);

                    //create displayed layout
                    var layout = _.reduce(precursors.reverse(), function (acc, itm, idx) {
                        var out = {
                            text: 'ROOT ' + itm,
                            type: itm,
                            id: '',
                            state: {
                                opened: true
                            },
                            children: acc
                        };
                        return [out];
                    }, network_layout);
                    
                    //populate assigned
                    console.log('ALL_SCREENS', pgstate.get('screens_inventory'));
                    var assigned = _.uniq(_.pluck(_.flatten(getAssignedScreens(network_layout)),'Id'));
                    //populate unassigned
                    var unassigned = _.map(_.filter(pgstate.get('screens_inventory'), function (itm, idx) {
                        return !_.contains(assigned, itm.Id);
                    }), function (itm, idx) {
                        return _.extend({
                            Type: 'Screen'
                        }, itm)
                    });
                    
                    //display layout
                    var unassigned_ct = [];
                    unassigned_ct.push({
                        "text": "UNASSIGNED",
                        "type": "Unassigned",
                        "id": "unassigned-root",
                        "state": {
                            "opened": true
                        },
                        "children": recursiveFormatter(unassigned)
                    });
                    
                    //TRACK TEMP STATE: DISPLAY LAYOUT
                    pgtempstate.set('display_layout', layout);
                    pgtempstate.set('display_unassigned', unassigned_ct);

                    context.partial('templates/home.template', {
                        NetworkId: getNetworkID()
                    }, function (context) {
                        //INIT sessions
                        InitSessionsPage(context);
                    });
                };

                //ROUTE: #/sessions, Sessions List
                this.get('#/sessions', function (context) {
                    if(!pgstate.exists('current_user')) {
                        this.redirect('#/');
                    }

                    //epoxy connect
                    x2o.api.epoxy.get().then(function(epoxy){
                        client = epoxy;
                        client.connect(undefined, 'controller').then(function () {

                            //register client
                            client.registerClient(cartridgeId, epoxy_events_handler);
                            client.sendCommand({
                                "Id": cartridgeId
                            },{
                                "Event": "REGISTER",
                                "Data": {
                                    "Type": "Administrator",
                                    "NetworkId": getNetworkID()
                                }
                            }).then(function (response) {
                                if(response.success) {
                                    //get screens
                                    client.sendCommand({
                                        "Id": cartridgeId
                                    },{
                                        "Event": "GET_ALL_SCREENS",
                                        "Data": {
                                            "Type": "Administrator",
                                            "NetworkId": getNetworkID()
                                        }
                                    }).then(function (response) {
                                        if(response.success) {
                                            pgstate.set('screens_inventory', response.data);
                                            //get layout
                                            client.sendCommand({
                                                "Id": cartridgeId
                                            }, {
                                                "Event": "GET_LAYOUT",
                                                "Data": {
                                                    "Type": "Administrator",
                                                    "NetworkId": getNetworkID()
                                                }
                                            }).then(function (response) {
                                                if(response.success) {
                                                    RenderSessionsPage(context, response);
                                                } else {
                                                    context.partial('templates/error.template', {
                                                        item: {
                                                            message: 'Layout Query Failed.'
                                                        }
                                                    });
                                                }
                                            });
                                        } else {
                                            context.partial('templates/error.template', {
                                                item: {
                                                    message: 'Screens Query Failed.'
                                                }
                                            });        
                                        }
                                    });
                                } else {
                                    context.partial('templates/error.template', {
                                        item: {
                                            message: 'Registration Failed.'
                                        }
                                    });
                                }
                            });
                            context.log("Epoxy client connected!");
                        })
                        .fail(function (err) {
                            context.log("Connection to server failed: " + err);
                            context.partial('templates/login.template', {
                                item: {
                                    message: "Connection to server failed: " + err
                                }
                            });
                        });
                    });
                });

                //POST: #/login, Login POST
                this.post('#/login', function (context) {
                    var current_user = _.extend({
                        Username: 'TESTUSER'
                    }, context.params);
                    pgstate.set('current_user', current_user);
                    pgcookie.set('current_user', current_user);                    
                    app.trigger('log.in');
                    return false;
                })

                //GET: #/logout, Logout GET
                this.get('#/logout', function (context) {
                    app.trigger('log.out');
                    return false;
                });

    

                //END----------

            });

            $(function () {
                app.run('#/');
            });

        });
    });
})();