//ACTION button field
var pgActionBtnField = function(config) {
    jsGrid.Field.call(this, config);
};
 
pgActionBtnField.prototype = new jsGrid.Field({
 
    css: "activity-field",            // redefine general property 'css'
    align: "center",              // redefine general property 'align' 
    itemTemplate: function(value, rowdata) {
        var out =
            '<div class="control-ct d-flex justify-content-around align-items-center flex-row">' 
            + '<button class="nav-link pg-action-btn" data-session-id="' + rowdata.Id + '" data-action-id="edit"' 
            + (rowdata.IsActive ? ' disabled' : '') + '>'
            + '<i class="fas fa-edit"></i>'
            + '</button>'
            + '<button class="nav-link pg-action-btn" data-session-id="' + rowdata.Id + '" data-action-id="clone">'
            + '<i class="far fa-clone"></i>'
            + '</button>'
            + '<button class="nav-link pg-action-btn" data-session-id="' + rowdata.Id + '" data-action-id="load"' 
            + (rowdata.IsActive ? '' : ' disabled') + '>'
            + '<i class="fas fa-sign-in-alt"></i>'
            + '</button>'
            + '<button class="nav-link pg-action-btn" data-session-id="' + rowdata.Id + '" data-action-id="view">'
            + '<i class="fas fa-eye"></i>'
            + '</button>'
            + '<button class="nav-link pg-action-btn" data-session-id="' + rowdata.Id + '" data-action-id="delete"'
            + (rowdata.IsActive ? ' disabled' : '') +'>'
            + '<i class="fas fa-trash-alt"></i>'
            + '</button>'
            + '</div>';
        return out;
    }
});

//ACTIVE switch field
var pgActiveToggleField = function(config) {
    jsGrid.Field.call(this, config);
};
 
pgActiveToggleField.prototype = new jsGrid.Field({ 
    css: "activity-field",            // redefine general property 'css'
    align: "center",              // redefine general property 'align'
    itemTemplate: function(value, rowdata) {
        var out = 
            '<div class="switch-ct d-flex align-items-center justify-content-center" style="height: 100%;"><label class="switch" style="margin: 0;">'
            + '<input class="activation-cb" type="checkbox" data-session-id="' + rowdata.Id + '"' + (rowdata.IsActive ? ' checked' : '') + '>'
            + '<span class="slider round"></span>'
            + '</label></div>';
        return out;
    }
});

//ASSIGN FIELD MODELS to TYPE
jsGrid.fields.activetoggle = pgActiveToggleField;
jsGrid.fields.actionslist = pgActionBtnField;